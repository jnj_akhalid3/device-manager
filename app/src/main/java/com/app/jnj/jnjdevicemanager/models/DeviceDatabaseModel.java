package com.app.jnj.jnjdevicemanager.models;

import com.google.gson.JsonObject;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

/**
 * Created by Adii on 9/9/2016.
 */

@DatabaseTable(tableName = "DeviceInfo")
public class DeviceDatabaseModel {

    @DatabaseField(generatedId = false)
    private int id;

    @DatabaseField
    private String device;

    @DatabaseField
    private String os;

    @DatabaseField
    private String manufacturer;

    @DatabaseField
    private String lastCheckedOutDate;

    @DatabaseField
    private String lastCheckedOutBy;

    @DatabaseField
    private Boolean isCheckedOut;

    public DeviceDatabaseModel(){}

    public DeviceDatabaseModel(int id, String device, String os, String manufacturer,
                               String lastCheckedOutBy, String lastCheckedOutDate,
                               Boolean isCheckedOut){
        super();
        this.id = id;
        this.device = device;
        this.os = os;
        this.manufacturer = manufacturer;
        this.lastCheckedOutBy = lastCheckedOutBy;
        this.lastCheckedOutDate = lastCheckedOutDate;
        this.isCheckedOut = isCheckedOut;

    }
    @Override
    public String toString() {
        try {
        JSONObject val = new JSONObject();
        val.put("id", id);
        val.put("device", device);
        val.put("os", os);
        val.put("device", manufacturer);
        val.put("lastCheckedOutBy", lastCheckedOutBy);
        val.put("lastCheckedOutDate", lastCheckedOutDate);
        val.put("isCheckedOut", isCheckedOut);

            return val.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "None";


    }


}
