package com.app.jnj.jnjdevicemanager.shared;

/**
 * Created by Adii on 9/8/2016.
 */
public class Constants {

    public static final String ENDPOINT = "http://private-1cc0f-devicecheckout.apiary-mock.com";
    // Used to switch between Fragments.
    public static enum Fragments
    {
        MAIN,
        DEVICE,
        DeviceManagement
    }

    public static final String FRAGMENT_TAG = "fragment_tag";
    public static final String MESSAGE_KEY = "message_key";



}
