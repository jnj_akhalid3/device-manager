package com.app.jnj.jnjdevicemanager.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.jnj.jnjdevicemanager.shared.Constants;
import com.app.jnj.jnjdevicemanager.Database.DeviceOpenDatabaseHelper;
import com.app.jnj.jnjdevicemanager.R;
import com.app.jnj.jnjdevicemanager.adapters.DeviceAdapter;
import com.app.jnj.jnjdevicemanager.interfaces.DeviceAPI;
import com.app.jnj.jnjdevicemanager.models.DeviceDatabaseModel;
import com.app.jnj.jnjdevicemanager.models.DeviceListItem;
import com.app.jnj.jnjdevicemanager.models.DeviceModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class DeviceFragment extends Fragment {

    private final String TAG = "DeviceFragment";

    private DeviceOpenDatabaseHelper deviceDbHelper;

    private ArrayList<DeviceListItem> list = new ArrayList<>();

    private DeviceAdapter deviceAdapter;

    private RuntimeExceptionDao<DeviceDatabaseModel, Integer> deviceDao;

    @BindView(R.id.deviceRecycler)
    RecyclerView deviceRecycler;

    private RecyclerView.LayoutManager layoutManager;

    public DeviceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "OncreateView");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_device, container, false);

        Bundle arguments = getArguments();
        if (arguments != null) {
            String message = arguments.getString("message");
        }

        // ButterKnife binding
        ButterKnife.bind(this, view);

        deviceDbHelper = OpenHelperManager.getHelper(this.getContext(), DeviceOpenDatabaseHelper.class);
        deviceDao = deviceDbHelper.gerDeviceRuntimeExceptionDao();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final DeviceAPI deviceAPI = retrofit.create(DeviceAPI.class);


        final Call<ArrayList<DeviceModel>> deviceListCall = deviceAPI.getDevices();
        layoutManager = new LinearLayoutManager(getContext());
        deviceListCall.enqueue(new Callback<ArrayList<DeviceModel>>() {
            @Override
            public void onResponse(Call<ArrayList<DeviceModel>> call, Response<ArrayList<DeviceModel>> response) {

                ArrayList<DeviceModel> deviceModel = response.body();
                if (list.isEmpty()) {
                    createOnResponseListData(deviceModel);


                }

                // Load data here.
                loadData(list);


            }

            @Override
            public void onFailure(Call<ArrayList<DeviceModel>> call, Throwable t) {
                Log.i("MAIN", "onFailure: Failed" + t);
                List<DeviceDatabaseModel> devices = deviceDao.queryForAll();
                if(list.isEmpty()) {
                    createCachedListData(devices);
                    // Load data here.
                }
                loadData(list);
            }

        });


        return view;
    }


    public void createOnResponseListData(ArrayList<DeviceModel> deviceModel){

        for (int x = 0; x < deviceModel.size(); x++) {
            Integer id = deviceModel.get(x).getId();
            String deviceName = deviceModel.get(x).getDevice();
            String os = deviceModel.get(x).getOs();
            String manufacturer = deviceModel.get(x).getManufacturer();
            String lastCheckedOutBy = "";
            String lastCheckedOutDate = "";
            Boolean isCheckedOut = deviceModel.get(x).getCheckedOut();
            String availabilityStatus;

            String phone_details = deviceName + " - " + os;

            if (isCheckedOut) {
                lastCheckedOutDate = deviceModel.get(x).getLastCheckedOutDate();
                lastCheckedOutBy = deviceModel.get(x).getLastCheckedOutBy();
                availabilityStatus = "Checked out by " + lastCheckedOutBy;
            } else {
                availabilityStatus = "Available";
            }

            DeviceListItem deviceListItem = new DeviceListItem(
                    phone_details, availabilityStatus, isCheckedOut,
                    manufacturer, lastCheckedOutBy, lastCheckedOutDate

            );
            try {
                deleteDeviceData("id", id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            createDeviceData(id,  deviceName,  os,  manufacturer,
                    lastCheckedOutBy,  lastCheckedOutDate,
                    isCheckedOut);

            list.add(deviceListItem);




            Log.i(TAG, "onResponse: " + list);
        }
    }
    public void createCachedListData(List<DeviceDatabaseModel> devicesList){
        String json = new Gson().toJson(devicesList);
        JsonParser jsonParser = new JsonParser();
        JsonElement je = jsonParser.parse(json);
        JsonArray array = je.getAsJsonArray();


        for (int x = 0;  x < array.size(); x++){
            JsonObject jsonRow = (JsonObject) array.get(x);
            int id = jsonRow.get("id").getAsInt();
            Log.i(TAG, "onResponse: " + id);
            String os = jsonRow.get("os").getAsString();
            Log.i(TAG, "onResponse: " + os);

            String manufacturer = jsonRow.get("manufacturer").getAsString();
            String device = jsonRow.get("device").getAsString();
            Boolean isCheckedOut = jsonRow.get("isCheckedOut").getAsBoolean();
            String lastCheckedOutBy = "";
            String lastCheckedOutDate = "";
            String availabilityStatus;

            String phone_details = device + " - " + os;


            if (isCheckedOut.toString().toLowerCase() == "true") {
                lastCheckedOutDate = jsonRow.get("lastCheckedOutDate").toString();
                lastCheckedOutBy = jsonRow.get("lastCheckedOutBy").toString();
                availabilityStatus = "Checked out by " + lastCheckedOutBy;
            } else {
                availabilityStatus = "Available";
            }

            DeviceListItem deviceListItem = new DeviceListItem(
                    phone_details, availabilityStatus, isCheckedOut,
                    manufacturer, lastCheckedOutBy, lastCheckedOutDate);
            Log.i(TAG, "onFailure: " + deviceListItem);
            list.add(deviceListItem);
            //Setting recycler view layout

        }
    }

    public void loadData(ArrayList<DeviceListItem> listData){

        layoutManager = new LinearLayoutManager(getContext());
        deviceRecycler.setLayoutManager(layoutManager);
        deviceRecycler.setHasFixedSize(true);
        deviceAdapter = new DeviceAdapter(listData);
        deviceRecycler.setAdapter(deviceAdapter);

    }



    public void createDeviceData(int id, String device, String os, String manufacturer,
                                 String lastCheckedOutBy, String lastCheckedOutDate,
                                 Boolean isCheckedOut) {

        deviceDbHelper = OpenHelperManager.getHelper(this.getContext(), DeviceOpenDatabaseHelper.class);
        deviceDao = deviceDbHelper.gerDeviceRuntimeExceptionDao();

        deviceDao.create(new DeviceDatabaseModel(id, device, os, manufacturer, lastCheckedOutBy,
                lastCheckedOutDate, isCheckedOut));
    }

    public void deleteDeviceData(String fieldName, int value) throws SQLException {

        deviceDbHelper = OpenHelperManager.getHelper(this.getContext(), DeviceOpenDatabaseHelper.class);
        deviceDao = deviceDbHelper.gerDeviceRuntimeExceptionDao();

        DeleteBuilder<DeviceDatabaseModel, Integer> deleteBuilder = deviceDao.deleteBuilder();
        deleteBuilder.where().eq(fieldName, value);
        deleteBuilder.delete();
    }


    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
