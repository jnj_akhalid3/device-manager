package com.app.jnj.jnjdevicemanager.models;

import com.google.gson.JsonElement;

/**
 * Created by Adii on 9/8/2016.
 */
public class DeviceListItem {




    public String getPhone_details() {
        return phone_details;
    }

    public void setPhone_details(String phone_details) {
        this.phone_details = phone_details;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getLastCheckedOutBy() {
        return lastCheckedOutBy;
    }

    public void setLastCheckedOutBy(String lastCheckedOutBy) {
        this.lastCheckedOutBy = lastCheckedOutBy;
    }

    public String getLastCheckedOutDate() {
        return lastCheckedOutDate;
    }

    public void setLastCheckedOutDate(String lastCheckedOutDate) {
        this.lastCheckedOutDate = lastCheckedOutDate;
    }


    public boolean isCheckedOut() {
        return isCheckedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        isCheckedOut = checkedOut;
    }

    private String phone_details, availability, manufacturer, lastCheckedOutBy,lastCheckedOutDate, key;
    private boolean isCheckedOut;
    public DeviceListItem(String phone_details, String availability,
                          boolean isCheckedOut, String manufacturer,
                          String lastCheckedOutBy, String lastCheckedOutDate){
        this.setPhone_details(phone_details);
        this.setAvailability(availability);
        this.setCheckedOut(isCheckedOut);
        this.setManufacturer(manufacturer);
        this.setLastCheckedOutBy(lastCheckedOutBy);
        this.setLastCheckedOutDate(lastCheckedOutDate);


    }
}
