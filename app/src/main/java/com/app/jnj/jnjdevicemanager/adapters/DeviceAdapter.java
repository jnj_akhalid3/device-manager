package com.app.jnj.jnjdevicemanager.adapters;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.jnj.jnjdevicemanager.R;
import com.app.jnj.jnjdevicemanager.UI.DeviceFragment;
import com.app.jnj.jnjdevicemanager.UI.DeviceManagementFragment;
import com.app.jnj.jnjdevicemanager.UI.MainActivity;
import com.app.jnj.jnjdevicemanager.models.DeviceListItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Adii on 9/8/2016.
 */
public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.DeviceViewHolder>{
    private Context context;
    private ArrayList<DeviceListItem> deviceArrayList = new ArrayList<>();

    public DeviceAdapter(ArrayList<DeviceListItem> deviceArrayList){
        Log.i("ADAPTER", "DeviceAdapter: " + deviceArrayList.toString());
        this.deviceArrayList = deviceArrayList;
        notifyDataSetChanged();
    }

    public DeviceAdapter() {
        this.deviceArrayList = null;
    }

    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        
        final View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_item, parent, false);
        final DeviceViewHolder deviceHolder = new DeviceViewHolder(viewItem);

        viewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeviceListItem device = deviceArrayList.get(deviceHolder.getAdapterPosition());

                //You can change the fragment, something like this, not tested, please correct for your desired output:
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                DeviceManagementFragment deviceManagementFragment = DeviceManagementFragment
                        .newInstance(device.getPhone_details(), device.getManufacturer(),
                                     device.getLastCheckedOutDate(), device.getLastCheckedOutBy(), device.isCheckedOut());
               //Create a bundle to pass data, add data, set the bundle to your fragment and:
                activity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, deviceManagementFragment)
                        .addToBackStack(null).commit();


            }
        });

        return deviceHolder;
    }

    @Override
    public void onBindViewHolder(DeviceViewHolder holder, int position) {
        DeviceListItem DEVICE = deviceArrayList.get(position);
        holder.phone_details_TV.setText(DEVICE.getPhone_details());
        holder.availability_TV.setText(DEVICE.getAvailability());

    }

    @Override
    public int getItemCount() {
        return deviceArrayList.size();
    }

    public static class DeviceViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.phone_details)
        TextView phone_details_TV;

        @BindView(R.id.deviceCardViewList)
        CardView deviceCardViewList;

        @BindView(R.id.availability)
        TextView availability_TV;

        public DeviceViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);

        }
    }
}
