package com.app.jnj.jnjdevicemanager.UI;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.app.jnj.jnjdevicemanager.R;
import com.app.jnj.jnjdevicemanager.shared.Constants;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.content_frame)
    FrameLayout content_frame;

    private String currentFrag;

    private Fragment switchFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        //Initial Fragment
        switchFragment(Constants.Fragments.DEVICE);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                // Add Device Fragment
            }
        });


    }



    public void switchFragment(Constants.Fragments fragment){

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (fragment){
            case  DEVICE:
                if (switchFragment == null) {
                    switchFragment = new DeviceFragment();
                    currentFrag = "Device";
                }
                break;
            case  DeviceManagement:
                detach(currentFrag);
                switchFragment = new DeviceManagementFragment();
                currentFrag = "DeviceM";
                break;

        }
        if (switchFragment != null){
            ft.replace(R.id.content_frame, switchFragment, currentFrag)
                    .addToBackStack(null)
                    .commit();


        }

    }


    public void detach(String fragment){
        switch (fragment){
            case "Device":
                DeviceFragment deviceFragment = new DeviceFragment();
                deviceFragment.onDetach();

        }
    }








    }
