package com.app.jnj.jnjdevicemanager.interfaces;

import com.app.jnj.jnjdevicemanager.models.DeviceModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Adii on 9/7/2016.
 */
public interface DeviceAPI {

    @GET("devices")
    Call<ArrayList<DeviceModel>> getDevices();

}
