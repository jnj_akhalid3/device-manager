package com.app.jnj.jnjdevicemanager.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.app.jnj.jnjdevicemanager.R;
import com.app.jnj.jnjdevicemanager.models.DeviceDatabaseModel;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.ReferenceObjectCache;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.InputStream;
import java.sql.SQLException;

/**
 * Created by Adii on 9/9/2016.
 */
public class DeviceOpenDatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "Device.db";
    private static final int DATABASE_VERSION = 1;

    /**
     * The data access object used to interact with the Sqlite database to do C.R.U.D operations.
     */
    private Dao<DeviceDatabaseModel, Integer> deviceDao = null;
    private RuntimeExceptionDao<DeviceDatabaseModel, Integer> deviceRuntimeDao = null;

    public DeviceOpenDatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }
    public DeviceOpenDatabaseHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
    }

    public DeviceOpenDatabaseHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion, int configFileId) {
        super(context, databaseName, factory, databaseVersion, configFileId);
    }

    public DeviceOpenDatabaseHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion, File configFile) {
        super(context, databaseName, factory, databaseVersion, configFile);
    }

    public DeviceOpenDatabaseHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion, InputStream stream) {
        super(context, databaseName, factory, databaseVersion, stream);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            /**
             * creates the Todo database table
             */
            TableUtils.createTable(connectionSource, DeviceDatabaseModel.class);
            Log.i("CREATE", "onCreate: Table created");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            /**
             * Recreates the database when onUpgrade is called by the framework
             */
            TableUtils.dropTable(connectionSource, DeviceDatabaseModel.class, false);
            onCreate(database, connectionSource);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<DeviceDatabaseModel, Integer> getDao() throws SQLException {
        if(deviceDao == null) {
            deviceDao = getDao(DeviceDatabaseModel.class);
            deviceDao.setObjectCache(ReferenceObjectCache.makeSoftCache());
        }
        return deviceDao;
    }


    public RuntimeExceptionDao<DeviceDatabaseModel, Integer> gerDeviceRuntimeExceptionDao(){
        if(deviceRuntimeDao == null){
            deviceRuntimeDao = getRuntimeExceptionDao(DeviceDatabaseModel.class);
        }
        return deviceRuntimeDao;
    }
    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
        deviceDao = null;
        deviceRuntimeDao = null;
    }

}
