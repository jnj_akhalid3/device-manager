package com.app.jnj.jnjdevicemanager.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.jnj.jnjdevicemanager.R;

import org.w3c.dom.Text;

import java.lang.reflect.Field;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeviceManagementFragment extends Fragment {


    @BindView(R.id.os_tv)
    TextView os_TV;

    @BindView(R.id.deviceName_tv)
    TextView deviceName_TV;

    @BindView(R.id.manufacturer_tv)
    TextView manufacturer_TV;

    @BindView(R.id.lastCheckedOut_tv)
    TextView lastCheckedOut_TV;


    @BindView(R.id.lastCheckedUser_tv)
    TextView lastCheckedUser_TV;

    @BindView(R.id.checkedPlaceHolderBtn)
    Button checkedPlaceHolderBtn;

    public DeviceManagementFragment() {
        // Required empty public constructor
    }

    public static DeviceManagementFragment newInstance(String phone_details,
                                                       String manufacturer,
                                                       String lastCheckedOutDate,
                                                       String lastCheckedOutUser ,
                                                       Boolean isCheckedOut) {

        Bundle args = new Bundle();

        String[] deviceInfo = phone_details.split("-");
        String deviceName = deviceInfo[0];
        String os = deviceInfo[1];
        args.putString("deviceName", deviceName);
        args.putString("os", os);
        args.putString("manufacturer", manufacturer);
        args.putString("lastCheckedOutDate", lastCheckedOutDate);
        args.putString("lastCheckedOutUser", lastCheckedOutUser);
        args.putBoolean("isCheckedOut", isCheckedOut);


        DeviceManagementFragment fragment = new DeviceManagementFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_device_management, container, false);

        Bundle arguments = getArguments();
        // ButterKnife binding
        ButterKnife.bind(this, view);
        if (arguments != null) {
            deviceName_TV.setText(arguments.getString("deviceName"));
            os_TV.setText(arguments.getString("os"));
            manufacturer_TV.setText(arguments.getString("manufacturer"));
            lastCheckedOut_TV.setText(arguments.getString("lastCheckedOutDate"));
            lastCheckedUser_TV.setText(arguments.getString("lastCheckedOutUser"));

            if(arguments.getBoolean("isCheckedOut")){

                checkedPlaceHolderBtn.setText("Check In");

            }
            else {
                checkedPlaceHolderBtn.setText("Check Out");
            }
        }

        return view;

    }


    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
