package com.app.jnj.jnjdevicemanager.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Adii on 9/8/2016.
 */
public class DeviceModel {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("device")
    @Expose
    private String device;

    @SerializedName("os")
    @Expose
    private String os;

    @SerializedName("manufacturer")
    @Expose
    private String manufacturer;

    @SerializedName("lastCheckedOutDate")
    @Expose
    private String lastCheckedOutDate;

    @SerializedName("lastCheckedOutBy")
    @Expose
    private String lastCheckedOutBy;

    @SerializedName("isCheckedOut")
    @Expose
    private Boolean isCheckedOut;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getLastCheckedOutDate() {
        return lastCheckedOutDate;
    }

    public void setLastCheckedOutDate(String lastCheckedOutDate) {
        this.lastCheckedOutDate = lastCheckedOutDate;
    }

    public String getLastCheckedOutBy() {
        return lastCheckedOutBy;
    }

    public void setLastCheckedOutBy(String lastCheckedOutBy) {
        this.lastCheckedOutBy = lastCheckedOutBy;
    }

    public Boolean getCheckedOut() {
        return isCheckedOut;
    }

    public void setCheckedOut(Boolean checkedOut) {
        isCheckedOut = checkedOut;
    }
}
