#
# generated on 2016/09/14 08:27:08
#
# --table-start--
dataClass=com.app.jnj.jnjdevicemanager.models.DeviceDatabaseModel
tableName=DeviceInfo
# --table-fields-start--
# --field-start--
fieldName=id
# --field-end--
# --field-start--
fieldName=device
# --field-end--
# --field-start--
fieldName=os
# --field-end--
# --field-start--
fieldName=manufacturer
# --field-end--
# --field-start--
fieldName=lastCheckedOutDate
# --field-end--
# --field-start--
fieldName=lastCheckedOutBy
# --field-end--
# --field-start--
fieldName=isCheckedOut
# --field-end--
# --table-fields-end--
# --table-end--
#################################
